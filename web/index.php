<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use CommerceGuys\Guzzle\Plugin\Oauth2\Oauth2Plugin;

require __DIR__ . "/../vendor/autoload.php";

$app = new Silex\Application();

/**
 * These settings don't seem to make any difference at all!!
 */
$app['session.storage.options'] = [
	'gc_maxlifetime'	=> 30,
	'cookie_lifetime'	=> 0
];
$app->register(new Silex\Provider\SessionServiceProvider());

/**
 * cheap and nasty way to separate yammer client_id and client_secret so as to not check into git
 */
$params = include(__DIR__ . "/../config/params.php");

/**
 * "Home" page that bounces if not logged in
 */
$app->get('/', function() use ($app) {
	if(!$app['session']->has('token')) return "<a href='/login'>Click here to log in (bounce to yammer SSO)</a>.</p>";
	return sprintf(
		"Logged in as %s. <a href='/logout'>Click here to log out</a>. <p><a href='/profile'>Click here to view basic profile data</a></p>", 
		$app['session']->get('name')
	);
});

/**
 * Bounces to yammer login if auth token not in session
 */
$app->get('/login', function() use ($app, $params) {
	if($app['session']->has('token')) return $app->redirect('/');
	$redirect_uri = "http://localhost.yammertest/tokenauth";
	return $app->redirect(sprintf("https://www.yammer.com/dialog/oauth?client_id=%s&redirect_uri=%s", $params['client_id'], $redirect_uri));
});

/**
 * Log out
 */
$app->get('/logout', function() use ($app) {
	$app['session']->invalidate();
	return $app->redirect('/');
});

/**
 * Receives redirect from yammer with code, then GETs auth token from API
 */
$app->get('/tokenauth', function(Request $request) use ($app, $params) {
	if($request->query->has('error')) {
		return sprintf("An error was returned: %s. <a href='/login'>Try again?</a>", $request->query->get('error_description'));
	}

	$code = $request->query->get('code');
	try {
		$http_client = new Guzzle\Http\Client();
		$http_request = $http_client->get('https://www.yammer.com/oauth2/access_token.json', [], [
			'query' => [
				'client_id'		=> $params['client_id'],
				'client_secret'	=> $params['client_secret'],
				'code'			=> $code
			]
		]);
		$http_response = $http_request->send();
	} catch (Exception $e) {
		echo $e->getMessage();
		exit;
	}

	$response_data = json_decode($http_response->getBody(), true);
	$app['session']->set('token', $response_data['access_token']['token']);
	$app['session']->set('name', $response_data['user']['full_name']);
	return $app->redirect('/profile');
});

/**
 * Test for using API and auth-ing with token
 */
$app->match('/search', function(Request $request) use ($app) {
	switch($request->getMethod()) {
		case 'GET':
			return "
				<form method='post' action='/search'>
					<input type='text' name='query'>
					<input type='submit' value='Search'>
				</form>
			";
			break;
		case 'POST':
			try {
				$http_client = new Guzzle\Http\Client();
				$access_token = ['access_token' => $app['session']->get('token')];
				$oauth2 = new Oauth2Plugin();
				$oauth2->setAccessToken($access_token);
				$http_client->addSubscriber($oauth2);
				$http_request = $http_client->get('https://www.yammer.com/api/v1/search.json', [], [
					'query' => [
						'search'	=> $request->request->get('query')
					]
				]);
				$http_response = $http_request->send();
			} catch(Exception $e) {
				echo $e->getMessage();
				exit;
			}
			return new Response($http_response->getBody(), 200, ['Content-Type' => 'application/json']);
			break;
	}
})->method('GET|POST');

$app->get('/profile', function(Request $request) use ($app) {
	if(!$app['session']->has('token')) return $app->redirect('/');
	try {
		$http_client = new Guzzle\Http\Client();
		$access_token = ['access_token' => $app['session']->get('token')];
		$oauth2 = new Oauth2Plugin();
		$oauth2->setAccessToken($access_token);
		$http_client->addSubscriber($oauth2);
		$http_request = $http_client->get('https://www.yammer.com/api/v1/users/current.json');
		$http_response = $http_request->send();
	} catch (Exception $e) {
		echo $e->getMessage();
		exit;
	}
	$data = json_decode($http_response->getBody(), true);
	// return new Response($http_response->getBody(), 200, ['Content-Type' => 'application/json']);
	return sprintf(
		"<p>Logged in as %s. <a href='/logout'>Click here to log out</a>.</p>
		<p><img src={$data['mugshot_url']}></p>
		<ul>
			<li>Name: {$data['full_name']}</li>
			<li>Network Name: {$data['network_name']}</li>
			<li>Hire Date: {$data['hire_date']}</li>
			<li>Birth Date: {$data['birth_date']}</li>
		</ul>",
		$app['session']->get('name')
	);
});



/**
 * Run app!!
 */
$app->run();